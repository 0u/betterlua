# betterlua

Betterlua is full of lua metaprogramming tricks to make your life easier!

# Features

Iteration over strings via `pairs` and `ipairs`
```lua
for char in pairs("string") do
  print(char)
end
```

Replicate a string with ease!
```lua
print("foo" * 4) -- foofoofoofoo
```

Helpers for formatting
```lua
printf("The answer is %d", 21 * 2)
errorf("Invalid type %s", type(10))
```

Index into strings
```lua
local str = "foo"
print("The first letter is " .. str[1])
```
