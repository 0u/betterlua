-- Formatting helpers
function printf(s, ...) print(s:format(...))    end
function errorf(s, ...) error(s:format(...), 2) end

getmetatable('').__mul = function(self, times)
  if type(times) ~= 'number' then
    error("bad argument #2 to '*' (expected number got " .. type(times) .. ')', 2)
  end

  return self:rep(times)
end

-- allow indexing into string values
getmetatable('').__index = function(self, index)
  if type(self) == 'string' and type(index) == 'number' then
    return string.sub(self, index, index)
  else
    -- we need to preserve the old __index (pointing to string) so that
    -- calling functions with :, eg ("foo"):rep(10) still works.
    return string[index]
  end
end

-- allow iteration over strings via pairs and ipairs
local string_iter = function(s, i)
  local i = 0

  return function()
    i = i + 1

    if i > s:len() then
      return nil
    else
      return i, s:sub(i, i)
    end
  end, i
end

local mkpairs = function(pf)
  return function(...)
    if type(({...})[1]) == 'string' then
      return string_iter(...)
    else
      return pf(...)
    end
  end
end

pairs = mkpairs(pairs)
ipairs = mkpairs(ipairs)
