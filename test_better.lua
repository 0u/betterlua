-- TODO:
-- Isolate state for different tests, currently depending on the outcome of one test
-- another can fail.

require 'better'

local test = {}

local p = function(v)
  if type(v) == 'table' then
    for i,v in pairs(p) do print(i,v) end
  else
    print(v)
  end
end

----------------------------------- TESTS -----------------------------------

function test.string_iter()
  local strings = {
    'foo',
    'bar',
    'this string'
  }

  for tc, s in ipairs(strings) do
    local i = 1

    for idx, char in pairs(s) do
      if char ~= s:sub(i, i) then
        errorf('want %s got %s [tc %d]', s:sub(i, i), char, tc)
      end

      if idx ~= i then
        errorf('want %d got %d [tc %d]', idx, i, tc)
      end

      i = i + 1
    end
  end
end

-- breaking from a loop should not screw up the iterator.
function test.string_iter_break()
  for _ in pairs('a string') do
    break
  end

  local want = 'foo'
  local got = ''
  for _, char in pairs(want) do
    got = got .. char
  end

  if got ~= want then
    errorf("wanted %q got %q", want, got)
  end
end

function test.string_mul()
  local tests = {
    ['foofoo']    = {'foo', 2},
    ['barbarbar'] = {'bar', 3},
    ['xxxx']      = {'x', 4},
  }

  for want, arg in pairs(tests) do
    local got = arg[1] * arg[2]
    assert(got == want,
      ('%q * %d should be %q (got %q)'):format(
        arg[1], arg[2], want, got))
  end
end

function test.string_index()
  local s = '123'
  assert(s[1] == '1')
  assert(s[2] == '2')
  assert(s[3] == '3')
end

----------------------------------- TESTS -----------------------------------

local pad = 25
for name, tc in pairs(test) do
  io.write('TEST ' .. name .. (' '):rep(pad-name:len()) .. '| ')
  local ok, err = pcall(tc)
  print(err or 'OK')
end
